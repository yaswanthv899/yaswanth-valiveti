package main

import (
	"bufio"
	"fmt"
	"os"
)

func main() {

	/*Declairing multiple strings*/
	str1 := " This is root"
	str2 := " \nThis is leaf"
	str3 := " \nThis is fruit"

	/*My name holds as string*/
	name := "Yaswanth"

	/* Input my name from the keyBoard */
	reader := bufio.NewReader(os.Stdin)
	fmt.Println(" Enter your city")
	city, _ := reader.ReadString('\n')
	fmt.Println("I am from: ", city)

	/*Getting a number to find it is between  1 to 10*/
	getting := bufio.NewReader(os.Stdin)
	fmt.Println(" Enter the number")
	number, _ := getting.ReadString('\n')
	fmt.Println(number)
	v := 90
	//fmt.Println(v)OOOOOOo
	if v <= 10 {
		fmt.Println(v)
	} else {
		fmt.Println("The number is not in range")
	}

	/* Calculating the age */

	age := 30
	currentYear := 2021
	fmt.Println(currentYear)
	year := currentYear - age
	fmt.Println("The year id birth is:", year)

	/*Defining array and using it*/
	arr := [11]int{0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10}
	fmt.Println(arr)

	arr1 := [3]string{"csk", "kpl", "mi"}
	fmt.Println(arr1)

	/*For loop for pritng 10 values*/
	for j := 1; j < 11; j++ {

		fmt.Println("The values of i are: ", j)
	}

	/* Here I am Printing my name and adress */
	fmt.Println("\nThe name is: Yaswanth")
	fmt.Println("\nThe adress is : Hickory in frisco")
	fmt.Println(str1, str2, str3)
	fmt.Println("\nstring holding name: ", name)

	/* Declaring a struct*/
	var guest house

	guest.noRooms = 1
	guest.price = 100.00
	guest.city = "Texas"

	fmt.Println("The rooms for the geust in the house are: ", guest.noRooms)
	fmt.Println("The price for adding a room in the house for the guest: ", guest.price)
	fmt.Println("The city in which the house is: ", guest.city)

	/*Printing map*/
	sage1 := map[string]string{
		"yaswanthghj": "cenetene",
		"Deepit":      "Amex",
		"Chaitanya":   "Humana",
	}

	sage := map[string]string{
		"yaswanth":  "cenetene",
		"Deepit":    "Amex",
		"Chaitanya": "Humana",
	}

	fmt.Println(employee(sage1))
	fmt.Println(employee(sage))

}

type house struct {
	noRooms int
	price   float64
	city    string
}

func employee(sage map[string]string) map[string]string {

	return sage

}
