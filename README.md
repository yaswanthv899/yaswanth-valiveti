package main

import (
	"fmt"
)

func main() {
 i, j := 35, 58

p := &i
fmt.Println(*p)
*p = 98
fmt.Println(i)

p = &j
*p = 6
fmt.Println(j)
}
