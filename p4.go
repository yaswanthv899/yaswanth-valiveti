package main

import (
	"fmt"
)

func sayMessage(msg string, idx int) {
	fmt.Println(msg)
	fmt.Println("The value of index is: ", idx)
}

func main() {
	for i := 0; i < 5; i++ {
		sayMessage(" Hello Go!", i)
	}
}
